defmodule OctoberTalks.Futbol do
    
    def open_file do
        case File.open("data/team.csv") do
            {:error,_} -> :no_existe
            {:ok,_} -> :existe
        end
    end

    def get_teams do
        #El simbolo de ! es para que la funcion arroje una exepcion en lugar de una dupla con el atomo :error
        File.stream!("data/team.csv")
        |> Stream.drop(1)
        |> Flow.from_enumerable
        |> Flow.map(&String.split(&1,","))
        |> Flow.partition
        |> Enum.reduce(Map.new, fn(values, map) -> add_team_to_map(map, values) end)    
    end
    
    defp add_team_to_map(map, values) do
        Map.put(map, Enum.at(values, 1), 
                     String.replace(Enum.at(values, 3), "\"",""))
    end

end