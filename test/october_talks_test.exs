defmodule OctoberTalksTest do
  use ExUnit.Case
  doctest OctoberTalks

  test "greets the world" do
    assert OctoberTalks.hello() == :world
  end

  test "The 2 is pair" do
    assert OctoberTalks.es_par?(2)==true
  end

  test "The 301 is not pair" do
    assert OctoberTalks.es_par?(301)==false
  end

  test "Solo 200 y 1004 son pares" do
    lista = [1004,3004,21,200]
    assert OctoberTalks.solo_pares_ordenados(lista) == [200,1004,3004]
  end

  test "Podemos encontrar a Juventus" do
    assert Map.get(OctoberTalks.Futbol.get_teams, "9885") == "Juventus"
  end


end
